/*search and print words in reverse order
*/
#include <stdio.h>
#include <string.h>
#define N 256

void EnterText(char str[]);
int SearchWords(char str[]);
void PrintWords(int Count);
char *pwords[N];

int main()
{
	char str[N] = { 0 };
	int Count = 0;
	EnterText(str);
	Count = SearchWords(str);
	PrintWords(Count);
	return 0;
}

void EnterText(char str[])
{
	printf("Enter your text:\n");
	fgets(str, N, stdin);
	str[strlen(str) - 1] = 0;
}

int SearchWords(char str[])
{
	int i = 0, inWord = 0, count = 0;
	while (str[i])
	{
		if (str[i] != ' ' && inWord == 0)
		{
			inWord = 1;
			pwords[count++] = str + i;
		}
		else
			if (str[i] == ' ' && inWord == 1)
				inWord = 0;
		i++;
	}
	return count;
}

void PrintWords(int Count)
{
	char *p;
	int i;
	printf("\nwords in reverse order:\n");
	for (i = --Count; i >=0; i--)
	{
		p = pwords[i];
		while ((*p != ' ') && (*p != '\0'))
			putchar(*p++);
		putchar(' ');
	}
	puts("");
}