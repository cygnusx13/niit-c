/*
 ============================================================================
 Name        : lab 2.6.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description : Programs deletes unnecessary spaces
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include<string.h>

#define LEN_STRING 80

int main()
{
   char string[LEN_STRING];
   int spacesStart=0;
   int spacesEnd=0;
   int spasesMidlle=0;
   int curStrLen=0;

   setlinebuf(stdout);
   printf("Enter string\n");
   gets(string);

   //Remove spaces from start
   int i=0;
   while(string[i]==' ' && i<LEN_STRING)
   {
      if (string[i] == ' ')
      {
         spacesStart = spacesStart+1;
      }
      i++;
   }

   //shift to the left
   for(int j=0;j<LEN_STRING;j++)
   {
      string[j]=string[j+spacesStart];
   }

   //Remove spaces from end
   //skip /0 and /r at the end
   int j=strlen(string)-2;
   while(string[j]==' ')
   {
      if (string[j] == ' ')
      {
         spacesEnd=spacesEnd+1;
      }
      j--;
   }
   string[j+1]='\0';
   curStrLen=j+1;

   int m=0;
   //Remove spaces from middle
   while( m<curStrLen)
   {
      if(string[m] == ' ')
      {
         while (string[m] == ' ')
         {
            spasesMidlle=spasesMidlle+1;
            m++;
         }
         if (spasesMidlle>1)
         {
            //shift to the left
            for(int i=m;i<=curStrLen ;i++)
            {
               string[i-spasesMidlle+1]=string[i];
            }
            m=0;
            curStrLen = strlen(string);
         }
       spasesMidlle=0;
      }
     m++;
   }
   printf("%s\n",string);

   return 0;
}
