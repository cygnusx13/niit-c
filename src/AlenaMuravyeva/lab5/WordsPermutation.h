
typedef struct RandPair RandPair;

struct RandPair
{
   unsigned int randNumber1;
   unsigned int randNumber2;
};

void getWords(char *userString,char**ptrArray );
RandPair generateRandPair();
void printWords();
