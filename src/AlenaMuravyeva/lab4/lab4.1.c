/*
 ============================================================================
 Name        : lab4.1.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :The programs displays the rows in descending order
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LETTERS 80
#define MAX_STRINGS 10

int main()
{
   char userString [MAX_STRINGS][MAX_LETTERS];
   char *p[MAX_STRINGS];
   int num=0;
   int len [MAX_STRINGS];
   int hold=0;
   char *save;

   setlinebuf(stdout);

   puts ("Enter strings:");
   while(num<MAX_STRINGS && *fgets(userString[num],MAX_LETTERS,stdin)!='\r')
   {
      p[num]=userString[num];
      num++;
   }
   for(int i=0;i<num;i++)
   {
      len[i]=strlen(p[i]);
   }
   for(int i=1;i<num;i++)
   {
      for(int j=0; j<num-i;j++)
      {
         if(len[j+1]<len[j])
         {
            hold=len[j];
            save=p[j];
            len[j]=len[j+1];
            p[j]=p[j+1];
            len[j+1]=hold;
            p[j+1]=save;
         }
      }
   }

   for (int i=0;i<num;i++)
   {
      printf("%s",p[i]);
   }

   return 0;
}
