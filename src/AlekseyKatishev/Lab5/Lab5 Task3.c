#define _CRT_SECURE_NO_WARNINGS
/*
3. �������� ���������, �������������� ��������� ������� �����
�� ������� ����� ������ ������ ���������� �����, ����� �������
� ����������, �� ���� ������ � ����� ����� �������� �� ������.
���������:
��������� ��������� ������������ ���������� ���� � ������ ��� �������
��. ��� ������ ������ ����������� �������� �� ����� � ����������� ����
����� ������� �����.
*/

#define M 256
#include<stdio.h>
#include<stdlib.h>
#include <windows.h>
#include <time.h>
FILE *in;
FILE *out;
char line[M];
int  cnt_letter=0;
char *p, *start_w;

int get_line()
{
	
	if (fgets(line, M, in) != NULL)
	{
		p = line;
		return 1;
	}
	else
		return 0;

}


void change_letter()
{
	int i,rnd;
	char buf;
	cnt_letter -= 2;//���������� ������ � ��������� �����
	for (i = 0; i < cnt_letter; i++)
	{
		rnd = rand() % cnt_letter;
		buf = *(start_w+i);
		*(start_w+i) = *(start_w + rnd);//��������� � ������� i ��������� ����� ������ ����� �� ��������� �� ������
		*(start_w + rnd) = buf;
	}
}

void find_word()
{
	while (*p != '\n')
	{
		if ((p == line && *p != ' ') || *(p - 1) == ' ')
		{
			start_w = p+1;
			cnt_letter=0;
		}
		if (*(p + 1) == ' ' || *(p + 1) == '\n' || *(p + 1) == '\t')
				change_letter();//������� ������������ ����
		p++;//��������� ���������
		cnt_letter++;//������� ���-�� ����� ����� �����
	}

}

int main()
{
	srand(time(NULL));
	in = fopen("ChangeWord.txt", "rt");
	out = fopen("NewWords.txt", "wt");
	if ((in == NULL) || (out == NULL))
	{
		perror("File:");
		return(1);
	}
	while (get_line())
	{
		find_word();//������ line
		fputs(line, out);//���������� ����� ���������
	}
	fclose(in);
	fclose(out);
	return 0;
}