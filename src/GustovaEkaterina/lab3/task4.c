// Write a program that finds the sum of the numbers in the input string

#include <stdio.h>
#include <string.h>

#define N 80
#define Digit 6

void GetLane(char *lane);
void SummedNum(char *lane);

int main()
{
	char userlane[N];
	GetLane(userlane);
	SummedNum(userlane);
    return 0;
}
void GetLane(char *lane)
{
	printf("Enter the string.\n");
	fgets(lane, N, stdin);
	lane[strlen(lane) - 1] = 0;
}
void SummedNum(char *lane)
{
	int i = 0;
	int countDigits = 0;
	int sumbuf = 0;
	int sumend = 0;
	while(1)
	{
		for (; lane[i] >= '1' && lane[i] <= '9'; i++)
		{
			sumbuf += lane[i] - '0';
			if (lane[i + 1] >= '1' && lane[i + 1] <= '9')
			{
				sumbuf *= 10;
				countDigits++;
			}
			if (countDigits > Digit)
			{
				sumend += sumbuf;
				sumbuf = 0;
			}
		}
		countDigits = 0;
		sumend += sumbuf;
		sumbuf = 0;
		i++;
		if (lane[i] == 0)
		{
			putchar('\n');
			printf("%d\n", sumend);
			break;
		}
	}
}