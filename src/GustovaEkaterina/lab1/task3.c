//Translation from radians to degrees and vice versa
#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>

int main()
{
	float value;
	char ident;
	char datarequest[] = "Enter the angle value.(In format valueD or valueR. D for degrees, R for radians.";
	const char *identsSize[] = { "D","R" };
	do
	{
		puts(datarequest);
		scanf("%f%c", &value, &ident);
		do
		{
			getchar();
		} while (value == EOF);
	} while (value <= 0 || ident != *identsSize[0] && ident != *identsSize[1]);
	const float coeffordegrees = 0.0174533;
	const float coefforradians = 57.2958;
	if (ident == *identsSize[0])
	{
		value *= coeffordegrees;
		printf("%fR\n", value);
	}
	else 
	{
		value *= coefforradians;
		printf("%fD\n", value);
	}
	return 0;
}