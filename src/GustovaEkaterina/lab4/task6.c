/*Write a program that asks for the number of relatives in the family,
and then allows you to enter the relative's name and age*/

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

#define NUM 10
#define NAME 20

int main()
{
	char buf[NUM][NAME];
	char *young;
	char *old;
	int bufage;
	int bufMAX = 0;
	int bufMIN = 100;
	int count;
	puts("Enter the number of the relatives.");
	scanf("%d", &count);
	while (count)
	{
		puts("Enter name and age:");
		scanf("%s %d", buf[count], &bufage);
		if (bufage > bufMAX)
		{
			bufMAX = bufage;
			old = buf[count];
		}
	   if (bufage < bufMIN)
		{
			bufMIN = bufage;
			young = buf[count];
		}
	   count--;
	}
	printf("Oldest:%s %d and Youngest: %s %d\n", old, bufMAX, young, bufMIN);
	return 0;
}