/*Write a program that sorts the lines, but uses
line, read from a text file. The result of the program also
recorded in the file.*/
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZESTRING 80
#define QUANTILYSTRINGS 10

int comp(const void *, const void *);

int main()
{
	FILE *fp;
	char *pointer[QUANTILYSTRINGS];
	char line[QUANTILYSTRINGS][SIZESTRING];
	int count = 0;
	int i = 0;
	fp = fopen("C:text.txt", "rt");
	if (fp == NULL)
	{
		perror("File:");
		return 1;
	}
	while (count < QUANTILYSTRINGS && fgets(line[count], SIZESTRING, fp))
	{
		line[count][strlen(line[count])] = '\0';
		pointer[count] = line[count];
		count++;
	}
	fclose(fp);
	fp = fopen("text_out.txt", "wt");
	if (fp == NULL)
	{
		perror("File:");
		return 1;
	}
	qsort(pointer, count, sizeof(pointer[0]), comp);
	for (i = 0; i < count; i++)
	{
		fputs(pointer[i], fp);
		fputs("\n", fp);
	}
	fclose(fp);
	return 0;
}
int comp(const void *i, const void *j)
{
	return strlen(*(char**)i) - strlen(*(char**)j);
}
