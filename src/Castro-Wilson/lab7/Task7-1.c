/*******************************************************************
 * Написать программу, создающую связанный список с записями о     *
 * регионах и их кодах в соответствии с содержанием файла данных   *
 * Замечание:                                                      *
 * Файл скачивается по адресу:                                     *
 * http://introcs.cs.princeton.edu/java/data/fips10_4.csv          * 
 * Программа должна поддерживать следующие функции:                *
 * (a) Формирование списка на основе данных файла.                 *
 * (b) Поиск и вывод всех данных по буквенному обозначению страны. *
 * (c) Поиск конкретного региона по названию.                      *
 *******************************************************************/

//#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct NAME_REC {
    char abbrev[3]; //iso
    char regCod[3]; //fips
    char region[50]; //name
};

typedef struct NAME_REC TNAME_REC;
typedef TNAME_REC * PNAME_REC;

struct ITEM {
    PNAME_REC name_rec;
    struct ITEM *next;
    struct ITEM *prev;
};

typedef struct ITEM TITEM;
typedef TITEM * PITEM;

PITEM createList(PNAME_REC name_rec);
PNAME_REC creatName(char *line);
PITEM addToTail(PITEM tail, PNAME_REC name_rec);
int countList(PITEM head);
PITEM findByAbbrev(PITEM head, char *name); //findByIso
PITEM findByRegion(PITEM head, char *city); //findByName
void printAllData(PITEM item);

int main() {
    FILE *fp;
    int count = 0;
    char buf[512];
    char menu[3]; // code_menu
    char search[50];

    PITEM head, tail, item;
    fp = fopen("fips10_4.csv", "rt");
    if (!fp) {
        perror("File names.csv:");
        exit(1);
    }
    fgets(buf, 512, fp);
    while (fgets(buf, 512, fp)) {
        if (count == 0) {
            head = createList(creatName(buf));
            tail = head;
        } else {
            tail = addToTail(tail, creatName(buf));
        }
        count++;
    }
    fclose(fp);

    printf("Total items: %d\n\n", countList(head));

    puts("Please select in search's menu:");
    puts("1 - search by abbreviation of the country");
    puts("2 - search by region");
    fgets(menu, 3, stdin);

    if (menu[0] == '1') {
        //(b) Поиск и вывод всех данных по буквенному обозначению страны.
        puts("\nPlease enter the country's abbreviation (for example CO):");
        fgets(search, 3, stdin);
        item = findByAbbrev(head, search);
        if (item == NULL)
            printf("Not found!\n");
        else
            printAllData(item);
    } else if (menu[0] == '2') {
        //(c) Поиск конкретного региона по названию.
        puts("\nPlease enter the name's city  between quotes (for example \"Amazonas\"):");
        fgets(search, 50, stdin);
        item = findByRegion(head, search);
        if (item == NULL)
            printf("Not found!\n");
        else
            printAllData(item);
    } else
        puts("This menu does not exist. Please enter again.");

    return 0;
}

PITEM createList(PNAME_REC name_rec) {
    PITEM item = (PITEM) malloc(sizeof (TITEM));
    item->name_rec = name_rec;
    item->prev = NULL;
    item->next = NULL;
    return item;
}

PNAME_REC creatName(char *line) {
    int i = 0;
    PNAME_REC rec = (PNAME_REC) malloc(sizeof (TNAME_REC));

    while (*line && *line != ',')
        rec->abbrev[i++] = *line++;

    rec->abbrev[i] = 0;
    line++;
    i = 0;

    while (*line && *line != ',')
        rec->regCod[i++] = *line++;

    rec->regCod[i] = 0;
    line++;
    i = 0;

    while (*line)
        rec->region[i++] = *line++;
    rec->region[i] = 0;
    return rec;
}

PITEM addToTail(PITEM tail, PNAME_REC name_rec) {
    PITEM item = createList(name_rec);
    if (tail != NULL) {
        tail->next = item;
        item->prev = tail;
    }
    return item;
}

int countList(PITEM head) {
    int count = 0;
    while (head) {
        count++;
        head = head->next;
    }
    return count;
}

PITEM findByAbbrev(PITEM head, char *iso_country) {
    while (head) {
        if (strcmp(head->name_rec->abbrev, iso_country) == 0)
            return head;
        head = head->next;
    }
    return NULL;
}

PITEM findByRegion(PITEM head, char *city) {
    while (head) {
        if (strcmp(head->name_rec->region, city) == 0)
            return head;
        head = head->next;
    }
    return NULL;
}

void printAllData(PITEM item) {

    if (item != NULL) {
        printf("country's abbreviation - ");
        puts(item->name_rec->abbrev);
        printf("Region code - ");
        puts(item->name_rec->regCod);
        printf("Name of Region- ");
        puts(item->name_rec->region);
    }
}
