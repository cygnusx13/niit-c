/************************************************************************
 * Программа, которая принимает от пользователя строку и выводит ее на  *
 * экран, перемешав слова в случайном порядке.                          *
 * Замечание:                                                           *
 * Программа должна состоять минимум из трех функций:                   *
 * a) printWord - выводит слово из строки (до конца строки или пробела) *
 * b) getWords - заполняет массив указателей адресами первых букв слов  *
 * c) main - основная функция.                                          *
 * by Wilson Castro                                                     *
 ************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

#define SIZE 256
#define SIZE2 80
#define TRUE 1
#define FALSE 0

int get_words(char *line, char** pline) {
    int i;
    int countW = 0;
    int inword = FALSE;

    for (i = 0; line[i] != '\0'; i++) {
        if (!inword && !isspace(line[i])) {
            pline[countW++] = &line[i];
            inword = TRUE;
        }
        if (inword && isspace(line[i]))
            inword = FALSE;
    }
    return countW;
}

void randoms(int *index, int countW) {
    int i;
    int nrand = 0;
    int nrand2 = 0;
    int temp = 0;

    srand(time(NULL));
    for (i = 0; i < countW; i++)
        index[i] = i;

    for (i = 0; i < countW; i++) {
        do {
            nrand = rand() % countW;
            nrand2 = rand() % countW;
        } while (nrand == nrand2);
        temp = index[nrand];
        index[nrand] = index[nrand2];
        index[nrand2] = temp;
    }
}

void printWord(char **pline, int *index, int countW) {
    int i, j;

    for (i = 0; i < countW; i++) {
        j = index[i];
        while (*pline[j] && *pline[j] != ' ')
            printf("%c", *pline[j]++);
        putchar(' ');
    }
    printf("\n");
}

int main() {
    int len = 0;
    int countW = 0;
    char *pline[SIZE] = {0};
    char line[SIZE] = {0};
    int index[SIZE2] = {0};
    
    printf("Input your string: ");
    fgets(line, SIZE, stdin);
    len = strlen(line);
    line[len - 1] = '\0';
    countW = get_words(line, pline);
    randoms(index, countW);
    printWord(pline, index, countW);
    return 0;
}
