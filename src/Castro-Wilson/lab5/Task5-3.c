/******************************************************************************
 * Программа, которая читает построчно текстовый файл и переставляет случайно *
 * слова в каждой строке.                                                     *
 * Замечание:                                                                 *
 * Программа открывает существующий тектстовый файл и читает его построчно.   *
 * Для каждой строки вызывается функция, разработанная в рамках задачи 1.     *                                                       *
 ******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <windows.h>

#define SIZE 256
#define TRUE 1
#define FALSE 0
int get_words(char *line, char** pline);
int lenWord(const char* pPalabras);
void randinLine(char* pPalabras[], int countW);

int main() {
    char str[SIZE] = {0};
    char* pPalabras[SIZE] = {0};
    int numwords; 
    FILE *fpintro;

    srand(time(NULL));
    fpintro = fopen("intro.txt", "rt");
    if (!fpintro) {
        perror("File:");
        return 1;
    }
    while (!feof(fpintro)) {
        str[0] = '\0';
        fgets(str, SIZE, fpintro);
        numwords = get_words(str, pPalabras);
        randinLine(pPalabras, numwords);
        printf(str);
    }
    printf("\n");
    fclose(fpintro);
    return 0;
}

int get_words(char *line, char** pPalabras) {
    int i;
    int countW = 0;
    int inword = FALSE;

    for (i = 0; line[i] != '\0'; i++) {
        if (!inword && !isspace(line[i])) {
            pPalabras[countW++] = &line[i];
            inword = TRUE;
        }
        if (inword && isspace(line[i]))
            inword = FALSE;
    }
    return countW;
}

int lenWord(const char* pPalabras) {
    int lenw = 0;

    while (*(pPalabras + lenw) != '\0' && !isspace(*(pPalabras + lenw))) {
        lenw++;
    }
    return lenw;
}

void randCaracterInWord(char* pPalabras) {
    int i, k, rnd, nletters;
    char temp;

    nletters = lenWord(pPalabras);
    for (k = 1; k < nletters - 1; k++) {
        rnd = rand() % (nletters - 2) + 1;
        temp = *(pPalabras + k);
        *(pPalabras + k) = *(pPalabras + rnd);
        *(pPalabras + rnd) = temp;
    }
}

void randinLine(char* pPalabras[], int countW) {
    int i;
    for (i = 0; i < countW; i++)
        randCaracterInWord(pPalabras[i]);
}