/****************************************************************************** 
 * написать программу ”Калейдоскоп”, выводящую на экран изображение,           * 
 * составленное из симметрично расположенных звездочек ’*’.                    *
 * Изображение формируется в двумерном символьном массиве, в одной его        * 
 * части и симметрично копируется в остальные его части.                      *
 * Замечание:                                                                 *
 * Решение задачи протекает в виде следующей последовательности шагов:        *
 * 1) Очистка массива (заполнение пробелами)                                  *
 * 2) Формирование случайным образом верхнего левого квадранта (занесение ’*’) *
 * 3) Копирование символов в другие квадранты массива                         *
 * 4) Очистка экрана                                                          *
 * 5) Вывод массива на экран (построчно)                                      *
 * 6) Временная задержка                                                      *
 * 7) Переход к шагу 1                                                        *
 *******************************************************************************/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <windows.h>
#define N 20
#define M 79
#define astr 70
#define SPACE ' '
#define STAR '*'
#define NSTARS 10

void clearArry(char (*arry)[M], int n) {
    int i, j;
    for (i = 0; i < n; i++) {
        for (j = 0; j < M; j++)
            arry[i][j] = SPACE;
    }
}

void randFillArry(char (*arry)[M], int n, int nstars) {
    int i, j, x;
    for (x = 0; x < nstars; x++) {
        i = rand() % (N / 2);
        j = rand() % (M / 2);
        if (arry[i][j] == SPACE) {
            arry[i][j] = STAR;
        }
    }
}

void copyStars(char (*arry)[M], int n) {
    int i, j;
    for (i = 0; i < n / 2; i++) {
        for (j = 0; j < M / 2; j++) {
            if (arry[i][j] == STAR) {
                arry[N - 1 - i][j] = STAR;
                arry[N - 1 - i] [M - 1 - j] = STAR;
                arry[i] [M - 1 - j] = STAR;
            }
        }
    }
}

void printArry(const char (*arry)[M], int n) {
    int i, j;
    for (i = 0; i < n; i++) {
        for (j = 0; j < M; j++) {
            putchar(arry[i][j]);
        }
        putchar('\n');
    }
}

int main() {
    int i;
    char arry[N][M];
    srand(time(NULL));
    for (i = 0; i < 5; i++) {
        clearArry(arry, N);
        randFillArry(arry, N, NSTARS);
        copyStars(arry, N);
        system("clear");
        printArry(arry, N);
        Sleep(3000);
    }
    return 0;
}