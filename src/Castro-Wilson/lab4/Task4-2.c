/*******************************************************************************
 * Программа, которая с помощью массива указателей выводит слова               *
 * строки в обратном порядке                                                   *
 * Замечание:                                                                  *
 * Вместе со строкой создается массив указателей на char, в который заносятся  *
 * адреса первых символов каждого слова (альтернатива - адреса первого и по-   * 
 * следнего символов). Затем мы организуем вывод новой строки, используя этот  *
 * массив из указателей.                                                       *
 * By Wilson Castro. 11.2016.                                                  *
 *******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define SIZE 256
#define MAX_WORDS 128
#define TRUE 1
#define FALSE 0

int main() {
    char  line[SIZE] = {0};
    int   i, j, k,
          cuonter = 0,
          firstLeter = 0,
          secondLeter = 0,
          firstWord = TRUE,
          secundWord = FALSE;
    char* pwords[MAX_WORDS],
        * ptr;

    printf("Введите строку для обработки\n");
    fgets(line, 256, stdin);
    j = strlen(line);
    line[j - 1] = '\0';

    for (i = 1; i < j; i++) {
        if (i - 1 == 0 && line[i - 1] != ' ') { // пойск первого слово 
            firstWord = TRUE;
            firstLeter = i - 1;
        } else
            if (line[i - 1] == ' ' && line[i] != ' ') { // пойск первого слово если до него есть пробел
            firstWord = TRUE;
            firstLeter = i;
        }
        if (line[i - 1] != ' ' && line[i] == ' ') { //цикл для случая окончаяния слова с пробелом 
            secundWord = TRUE;
            secondLeter = i - 1;
        }
        if (line[i - 1] != ' ' && line[i] == '\0') { //цикл для случая окончаяния слова и строка 
            secundWord = TRUE;
            secondLeter = i - 1;
        }

        if (firstWord == TRUE && secundWord == TRUE) { // цикл для запольнения массива указателей  
            pwords[cuonter] = &line[firstLeter];
            cuonter++;
            firstWord = FALSE;
            secundWord = FALSE;
        }
    }

    for (i = cuonter - 1; i >= 0; i--) {
        ptr = pwords[i];
        while (*ptr != ' ' && *ptr != '\0') {
            putchar(*ptr);
            ptr++;
        }
        putchar(' ');
    }
    return 0;
}
