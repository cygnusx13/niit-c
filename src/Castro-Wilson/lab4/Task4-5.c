/*****************************************************************************  
 * Написать программу, сортирующую строки (см. задачу 1), но использующую    *
 * строки, прочитанные из текстового файла. Результат работы программы также *
 * записывается в файл.                                                      *
 * By Wilson Castro. 12.2016.                                                *
 *****************************************************************************/
#include <stdio.h>
#include <string.h>

#define N 20
#define M 256

int main() {
    char lines[N][M] = {0};

    char* plines[N];
    char* tmp;

    FILE* fpinput,
        * fpoutput;

    int i = 0,
            j = 0;

    fpinput = fopen("in.txt", "rt");
    fpoutput = fopen("out.txt", "wt");

    if (fpoutput == NULL || fpinput == NULL) {
        perror("Imposible to read or to write file");
        return 1;
    }

    i = 0;
    while (i < N && !feof(fpinput)) {
        fgets(lines[i], M, fpinput);
        i++;
    }
    for (i = 0; i < N; plines[i] = lines[i], i++)
        ;


    for (i = 0; i < N; i++)
        for (j = i; j < N; j++)
            if (strlen(plines[i]) > strlen(plines[j])) {
                tmp = plines[i];
                plines[i] = plines[j];
                plines[j] = tmp;
            }

    for (i = 0; i < N; i++){
        fputs(plines[i], fpoutput);
        puts(" ");
    }
    printf("Good Look!\n");

    fclose(fpinput);
    fclose(fpoutput);

    return 0;
}