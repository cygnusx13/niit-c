/******************************************************************************
 * Написать программу, которая запрашивает количество родственников в семье,  *
 * а потом позволяет ввести имя родственника и его возраст. Программа должна  *
 * определить самого молодого и самого старого родственника.                  *
 * Замечание:                                                                 *
 * Нужно завести массив строк для хранения имён и два указателя: young и old, *
 * которые по мере ввода, связывать с нужными строками. 
 * By Wilson Castro. 12.2016.                        *
 ******************************************************************************/
#include <stdio.h>
#define N 10
#define M 80

int main() {
    int     relatives = 0,
            youngest = 150,
            oldest = 0,
            curedge = 0,
            i;
    char*   young,
            * old;
    char    names[N][M] = {0};

    printf("Digite la cantidad de familiares que desea, deben ser menor de 10: ");
            scanf("%d", &relatives);
    for (i = 1; i <= relatives; i++) {
        printf("Digite el nombre del %d familiar y su edad: ", i);
        scanf("%s %d", names[i], &curedge);
        if (curedge < youngest) {
            young = names[i];
            youngest = curedge;
        }
        if(curedge>oldest){
            old = names[i];
            oldest=curedge;
        }
    }
            printf("El familiar mas joven es %s y tiene %d anios.\n", young, youngest);
            printf("EL familiar mas lonjevo es %s y tiene %d anios.\n", old, oldest);
    return 0;
}



