/*6. �������� ���������, ��������� ������ �� ������ ��������. ������� ���-
������ ������� � ������ ������, � ����� ������ � ������� ����� �������,
���� �� ���������� ������ 1.
���������:
� ������ ��������� ��������� ��������� �������������� �������, �� ����
���������� ���������� � �������� ������. ����� ���������� ��������� ���-
����� �� �����.*/

#include<stdio.h>
#define N 100

int main()
{ 
	int i, start_j, j, len;
	char arr[N];
	while (1)
	{
		printf("Please, enter string (max length %d)\n", N);
		fgets(arr,N,stdin);
		len = strlen(arr);
		if(len>N)
			printf("Try again (max length %d)!!!!!!!!!!!!\n", N);
		else
			break;
	}
	arr[len-1] = '\0'; // ������� ������ ������� ������
	len--;

	printf("Start:  \'%s\' - %d\n", arr, len);
	for(i=0; i<len; i++)
	{
		// len-1 - ��������� ������ ������ ����� \0
		if(arr[i]==' ' && (i == 0 || i == len-1 || arr[i+1] == ' '))
		{
			start_j = (i == 0 || i == len-1) ? i+1 : i+2;
			for(j=start_j; j<len; j++)
				arr[j-1] = arr[j];
			len--;
			arr[len] = '\0';
			i--;
		}
	}
	printf("Finish: \'%s\' - %d\n", arr, len);
	return 0;
}