/*1. �������� ���������, ������� ��������� ������������ ������ �������-
�� ����� � ����������, � ����� ��������� �� � ������� ��������-
��� ����� ������.
���������: ������ �������� �� ��������� ������ ������ � ������������ � ���������
���������� ������. ������������ ���������� �������� � ���������� �������
���������� �� char. ����� ��������� ����� ��������� ��������� �������� �
������� � ������� ������ � ������������ � ���������������� �����������.*/
#include<stdio.h>
#include<string.h>
#define ROW_NUM 100
#define ROW_LEN 256

int main()
{
	char ent_arr[ROW_NUM][ROW_LEN];
	char *p_arr[ROW_NUM], *p_tmp;
	int i, j, rows_num = 0, row_len=0;
	printf("Please, enter strings for sort, max string length is %d and max string count is %d (empty string for finish)\n", ROW_NUM, ROW_LEN);
	while(1)
	{
		fgets(ent_arr[rows_num], ROW_LEN, stdin);
		row_len = strlen(ent_arr[rows_num]);
		ent_arr[rows_num][row_len-1] = '\0';
		row_len--;
		p_arr[rows_num] = ent_arr[rows_num];
		if(row_len == 0 || rows_num >= ROW_NUM-1)
			break;
		rows_num++;
	}
	printf("Old array\n");
	for(i=0;i<rows_num;i++)
		printf("%d - '%s'\n", i, p_arr[i]);
	for(i=0; i<rows_num;i++)
	{
		for(j=1; j<rows_num-i;j++)
		{
			if(strlen(p_arr[j])> strlen(p_arr[j-1]))
			{
				p_tmp = p_arr[j];
				p_arr[j] = p_arr[j-1];
				p_arr[j-1] = p_tmp;
			}
		}
	}
	printf("New array\n");
	for(i=0;i<rows_num;i++)
		printf("%d - '%s'\n", i, p_arr[i]);
	return 0;
}

