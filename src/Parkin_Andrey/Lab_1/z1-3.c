/*3. �������� ���������, ������� ��������� �������� ���� �� �����-
��� � �������, �, ��������, � ����������� �� ������� ��� �����.
��������: 45.00D - �������� �������� � ��������, � 45.00R - �
��������. ���� ������ �������������� �� ������� %f%c*/
#include <stdio.h>
#define Pi 3.141592

int main()
{
	float value;
	char format;
	do
	{
		printf("Enter value of angle (45.00D as degree, 45.00R as radians)\n");
		scanf("%f%c",&value, &format);
		if(format !='D' && format!='R')
			printf("Wrong format (enter D for degree or R for radians)\n");
		else if(value < 0 || value >1000)
			printf("Wrong value (from 0.00 to 1000.00)\n");
		else
			break;
	}
	while(1);
	if(format == 'D')
		printf("Result is %f radians\n",(Pi*value)/180);
	else
		printf("Result is %f degree\n",(180*value)/Pi);
	return 0;
}