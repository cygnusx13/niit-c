#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main()
{
	float ugol;
	char ch;
	
	while (1)
	{
		printf("Vvedite ugol: ");
		scanf("%f%c", &ugol, &ch);
		if (ch == 'D' || ch == 'd')
		{
			printf("%f gradus = %f radian\n", ugol, 3.14*ugol / 180);
			break;
		}
			
		else if (ch == 'R' || ch == 'r')
		{
			printf("%f radian = %f gradus\n", ugol, 180 * ugol / 3.14);
			break;
		}
		else 
		{
			printf("data error! Vvedite v formate 'R', 'r', 'D', 'd' \n");
		}
	}
	return 0;
}