/*Практикум 1. Задача 1.
Написать программу, которая запрашивает у пользователя пол,
рост и вес, а затем анализирует соотношение роста и веса, выда-
вая рекомендации к дальнейшим действиям (похудеть, потолстеть,
норма)
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
char gender;
int height,weight,idealWeight=0;

void cleanIn() 
{
	char c;
	do 
	{
		c=getchar();
	} 
	while(c!='\n' && c!=EOF);
	
}

int main()
{
	while(1)
	{
		printf("Input your gender M/W:\n");
		scanf("%c",&gender);
		if(gender=='M'||gender=='m'||gender=='W'||gender=='w')
			break;
		else
		{
			puts("Input error!");
			cleanIn();
		}
	}

	while(1)
	{
		printf("Input your height, cm:\n");
		scanf("%d",&height);
		if(height>0&&height<220)
			break;
		else
		{
			puts("Input error!");
			cleanIn();
		}
	}

	while(1)
	{
		printf("Input your weight, kg:\n");
		scanf("%d",&weight);
		if(weight>0&&weight<150)
			break;
		else
		{
			puts("Input error!");
			cleanIn();
		}
	}

	if(gender=='M'||gender=='m')
	{
		idealWeight=height-100;
	}
	else
	{
		idealWeight=height-110;
	}

	if(weight==idealWeight)
		printf("You weight is normal.\n");
	else if(weight > idealWeight)
		printf("You need to lose weight.\n");
	else
		printf("You need to get well.\n");

	return 0;
}