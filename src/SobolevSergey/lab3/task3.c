/* Практикум №3. Задача 3.
Написать программу, которая для введённой строки выводит самое длинное
слово и его длину.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define N 256

int main( )
{
	char string[N];
	int i,count=0,max=0, startWord=0;

	puts("Enter a string:");
	fgets(string,N,stdin);

	for(i=0;i<strlen(string);i++)
		if(string[i]!=' ') 
			count++;
		else 
		{
			if(count > max) 
			{
				max=count;
				startWord=i-count;
			}

			count=0;
		}

	if(count > max) 
	{
		max=count;
		startWord=i-count;
	}

	for(i=startWord; i<startWord+max; i++)
		putchar(string[i]);
	
	printf(" - %d \n",max);

	return 0;
}
