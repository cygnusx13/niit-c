/* Практикум 4. Задача 1.
Написать программу, которая позволяет пользователю ввести несколь-
ко строк с клавиатуры, а затем выводящую их в порядке возраста-
ния длины строки.
Замечание:
Строки вводятся до появления пустой строки и записываются в двумерный
символьный массив. Одновременно происходит создание и заполнение массива
указателей на char. После окончания ввода программа сортирует укзатели в
массиве и выводит строки в соответствии с отсортированными указателями.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

#define N 5
#define M 80

int main( )
{
	char string[N][M]={0};
	char* pstr[N];
	char* tmp;
	int i=0,j;

	puts("Enter the strings:");
	while(i<N && (pstr[i]=*(string+i)))
		i++;

	i=0;
	while(i<N && *fgets(*(string+i),M,stdin)!='\n')
		i++;

	for(i=0; i < N; i++)
		for(j=i; j < N; j++)
			if(strlen(pstr[i]) > strlen(pstr[j]))
			{
				tmp=pstr[i];
				pstr[i]=pstr[j];
				pstr[j]=tmp;
			}

	printf("\n");

	for(i=0; i < N; i++)
		printf(pstr[i]);

	return 0;
}