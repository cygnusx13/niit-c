/* Практикум 4. Задача 3.
Написать программу, которая запрашивает строку и определяет, не является
ли строка палиндромом (одинаково читается и слева направо и справа налево)
Замечание:
Цель задачи - применить указатели для быстрого сканирования строки с двух
концов

*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define M 256
      
int main( )
{
	char string[M];
	puts("Enter a string:");
	fgets(string,M,stdin);

	int len=strlen(string)-1;
	char *startString=string;
	char *endString=&string[len-1];

	while(startString < endString)
	{
		if(*startString!=*endString) 
		{
			printf("The string is not a palindrome!\n");
			return 0;
		}
		*startString++;
		*endString--;
	}

	printf("The string is a palindrome!\n");
	return 0;
}