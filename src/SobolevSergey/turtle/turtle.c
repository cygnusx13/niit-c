/* Turtle. Черепашка.
Задача о черепашке. В 2х мерном массиве N*M из верхнего левого угла прийти
в нижний правый угол с МАКСИМАЛЬНОЙ суммой всех элементов.
можно двигаться вниз и вправо..
Сделать функции:
1. fillRand - массив N*M заполняется случайными числами
2. print - вывод на экран получившейся матрицы
3. sum - функция получает на вход 1ю матрицу
		на выходе дает матрицу СУММ ячеек
4. pathfinder - следопыт - поиск пути в обратном направлении
5. pathprint - визуализация пути Черепашки, из угла в угол

(пока без функций).
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <Windows.h>
#define N 16							// по горизонтали 
#define M 8								// по вертикали
HANDLE hstdout;							// "раскраска" текста через функцию Windows.h

int main( )
{
	int array[N][M];					// объ¤вление двухмерного 1го массива N на M
	int arraySum[N][M];					// объ¤вление двухмерного 2го массива N на M, суммы пред элементов
	int adress[(N+M-1)*2]={0};			// адреса MAX — пути по которым побежит Черепашка
	int len=(N+M-1)*2;					// длина массива adress
	int i,j,k=0,temp=0;

	srand(time(NULL));

	hstdout=GetStdHandle(STD_OUTPUT_HANDLE);

	/******************************* fillRand - заполнение массива случайными числами *******************/
	for(i=0; i<N; i++)
		for(j=0; j<M; j++)
			array[i][j]=rand()%10;
	/******************************* print - вывод массива случайных чисел *****************************/
	printf("Array of random numbers: \n");
	for(i=0; i<N; ++i)
	{
		for(j=0; j<M; ++j)
			printf("%03d  ",array[i][j]);
		printf("\n");
	}
	printf("\n");
	/******************************* sum - заполнение второй матрицы **********************************/
	arraySum[0][0]=array[0][0];								// 1й элемент матр сумм 
															// присваиваем 1й элемент матр случ чисел
	for(i=0,j=1;j<M;j++)									// заполняем верхний ряд
		arraySum[i][j]=arraySum[i][j-1]+array[i][j];

	for(i=1,j=0;i<N;i++)									// заполн¤ем левый столбец
		arraySum[i][j]=arraySum[i-1][j]+array[i][j];

	for(i=1;i<N;i++)										// заполняем остальные ячейки -> в право и в низ
		for(j=1;j<M;j++)
			if(arraySum[i-1][j]>arraySum[i][j-1])
				arraySum[i][j]=array[i][j]+arraySum[i-1][j];
			else
				arraySum[i][j]=array[i][j]+arraySum[i][j-1];

	/******************************* print sum - вывод массива 2й матрицы для проверки******************/
	printf("Print arraySum: \n");
	for(i=0; i<N; ++i)
	{
		for(j=0; j<M; ++j)
			printf("%03d  ",arraySum[i][j]);
		printf("\n");
	}
	printf("\n\n");
	/******************************* pathfinder - поиск пути в обратном направлении******************/
	i=N-1;
	j=M-1;
	
	adress[0]=N-1;
	adress[1]=M-1;

	k=2;

	while(i!=0&&j!=0)					// while(i>=0&&j>=0)		while(i>0&&j>0)
	
	{
		if(i>0&&j>0)
		{
			if(arraySum[i-1][j]>arraySum[i][j-1])
				i--;
			else
				j--;
		}
		adress[k++]=i;
		adress[k++]=j;
	}
	
	while((i==0&&j!=0)||(i!=0&&j==0))
	{
		if(i==0)
			j--;
		else if(j==0)
			i--;
	
		adress[k++]=i;
		adress[k++]=j;
	}

	adress[k++]=0;
	adress[k++]=0;
	
	printf("\n");
	/******************************* печать адресов i и j вычесленной "обратной дороги" для проверки ******************/
	printf("Print adress: \n");
	for(k=0; k<len;k++)
	{
		if(k%2!=1)
		{
			printf("%d  ",adress[k]);

		}
		else
			printf("%d|",adress[k]);
	}	

	printf("\n\n");
	/******************************* переварачиваем массив adress**************************************************/
	j=len/2;
	k=0;
	for(int i=0; i<j; i++)
	{
		k=adress[i];
		adress[i]=adress[len-1-i];
		adress[len-1-i]=k;
	}
	k=0;
	j=0;
	for(int i=0; i<len; i=i+2)
	{
		k=adress[i];
		j=adress[i+1];
		adress[i]=j;
		adress[i+1]=k;
	}
	/******************************* печать перевернутого массива adress *****************************************/
	printf("Print revers adress: \n");
	for(k=0; k<len;k++)
	{
		if(k%2!=1)
		{
			printf("%d  ",adress[k]);

		}
		else
			printf("%d|",adress[k]);
	}

	printf("\n\n");

	/******************************* pathprint - визуализация исходного пути******************/
	printf("Print path: \n");
	k=0;
	SetConsoleTextAttribute(hstdout,0x3F);
	for(i=0; i<N; ++i)
	{
		for(j=0; j<M; ++j)
		{
			if(i==adress[k]&&j==adress[k+1])
			{
				SetConsoleTextAttribute(hstdout,0xF0);
				printf("(%03d)  ",arraySum[i][j]);
				SetConsoleTextAttribute(hstdout,0x3F);
				k=k+2;
			}
			else
				printf("%05d  ",arraySum[i][j]);
		}
		printf("\n");
	}
	printf("\n");
	SetConsoleTextAttribute(hstdout,0x5F);

	return 0;
}