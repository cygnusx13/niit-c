/* Практикум 2. Задача 7. 
Написать программу, выводящую таблицу встречаемости символов для введен-
ной пользователем строки. В этой таблице содержится символ строки и число
его повторений.
Замечание:
В этой программе мы стремимся к экономии времени, так что использование
дополнительных массивов оправдано.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

#define N 256

int main()
{
	char string[N];
	int count[N]={0};
	int i=0;

	puts("Enter a string:");
	fgets(string,N,stdin);
	string[strlen(string)-1]=0;


//********************* 1й  вариант решения    ************************
	for(i=0;string[i];i++)
	{
		count[string[i]]++;
	}

//********************* 2й  вариант решения    ************************
/*	do
		count[string[i++]]++;
	while(string[i]);
*/	

	for(i=0;i<N;i++)
	{
		if(count[i]!=0)
			printf("%c - %d \n",i,count[i]);
	}

	return 0;
}