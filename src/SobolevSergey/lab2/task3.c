/* Практикум 2. Задача 3. 
Написать программу, выводящую на экран треугольник из звёздочек
Замечание:
Треугольник должен выглядеть так:
  *
 ***
*****
Количество строк задаётся пользователем с клавиатуры.*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main()
{
	int i, j, n, p;

	printf("Enter the number of rows: ");
	scanf("%d", &n);

	for (i=1; i<=n; i++)
	{
		for (p=n-i; p>0; p--)
			putchar(' '); 
		
		for (j=i*2-1; j>0; j--)
			putchar('*');
			
		putchar('\n');
	}

	return 0;
}