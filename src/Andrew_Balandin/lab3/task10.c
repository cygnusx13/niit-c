/*
Написать программу, которая запрашивает у пользователя строку, состоящую
из нескольких слов и целое число n, а затем удаляет n - ое слово из строки. В
случае неккоректного n выводится сообщение об ошибке
Замечание:
В результате работы программы символы должны быть удалены из массива.
Создавать дополнительные массивы нельзя
 */

#include <stdio.h>
#include <ctype.h>

#define N 256
#define TRUE 1
#define FALSE 0

int main()
{
    int     i = 0,
            j = 0,
            n = 0,
            k = 0,
            nword = 0,
            inword = FALSE;
    
    char    str[N] = {0};
    
    printf("Input your text: ");
    fgets(str, N, stdin);
    
    printf("Input number of word: ");
    scanf("%d", &n);
    
    for(i = 0; str[i] != '\0'; i++)
    {
        if(!isspace(str[i]) && !inword)
        {
            inword = TRUE;
            nword++;
        }
        if(isspace(str[i]) && inword)
        {
            inword = FALSE;
            if(nword == n)
            {
                j = i - 1;
                while(!isspace(str[j]) && j >= 0)
                    j--,k++;
                for(j++; str[j + k] != '\0'; j++)
                    str[j] = str[j + k];
                str[j] = '\0';
                puts(str);
                return 0;
            }
        }
    }

    printf("Not number for word!!!\n");
    return 0;
}