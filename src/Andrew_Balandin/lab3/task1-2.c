/*
Написать программу, подсчитывающую количество слов во введён-
ной пользователем строке

*/

#include <stdio.h>
#include <ctype.h>

#define N 256
#define TRUE 1
#define FALSE 0

int main()
{
    char str[N] = {0};
    int inword = FALSE,
        i = 0,
        n = 0;
    
    printf("Input text and then put EOF on new line:\n\n\n");
    for(i = 0; (str[i] = getchar()) != EOF && i < N; i++)
    {
        if(!isspace(str[i]) && !inword)
        {
            inword = TRUE;
            n++;
        }
        if(isspace(str[i]) && inword)
            inword = FALSE;
    }

    printf("\n\n\nIn your text %d words!\n", n);

    return 0;
}