/*
Написать программу, выводящую на экран треугольник из звёздочек
Замечание:
Треугольник должен выглядеть так:
*
***
*****
Количество строк задаётся пользователем с клавиатуры.
*/

#include <stdio.h>
#define SPACE "   "
#define STAR " * "

int main()
{
    unsigned    n = 1, 
                i, 
                j;
    
    printf("Input number of rows: ");
    scanf("%u", &n);
    
    for(i = n; i >= 1; i--)
    {
        for(j = 1; j < i; j++)
            printf(SPACE);
        for(j = 1; j <= (n - i) * 2 + 1; j++)
            printf(STAR);
        printf("\n");
    }
    
    return 0;
}