/*
Написать программу, которая с помощью массива указателей выводит слова
строки в обратном порядке
Замечание:
Вместе со строкой создается массив указателей на char, в который заносятся
адреса первых символов каждого слова (альтернатива - адреса первого и по-
следнего символов). Затем мы организуем вывод новой строки, используя этот
массив из указателей.
*/


#include <stdio.h>
#include <strings.h>

#define N 256
#define TRUE 1
#define FALSE 0

int main()
{
    int i = 0,
        j = 0,
        inword = FALSE;
        
    char str[N] = {0};
    
    char* pwords[N];
    
    
    printf("Input your string :");
    fgets(str, N, stdin);
    for(i = 0; str[i] != '\0'; i++)
    {
        if(!isspace(str[i]) && !inword)
        {
            inword = TRUE;
            pwords[j] = &str[i];
            j++;
        }
        if(isspace(str[i]) && inword)
            inword = FALSE;
    }
    j--;
    while(j >= 0)
    {
        for(i = 0; !isspace(*(pwords[j] + i)); i++)
            putchar(*(pwords[j] + i));
        putchar(' ');
        j--;
    }
    putchar('\n');
    return 0;
}